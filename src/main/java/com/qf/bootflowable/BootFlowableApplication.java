package com.qf.bootflowable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootFlowableApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootFlowableApplication.class, args);
    }

}
