package com.qf.bootflowable;

import liquibase.pro.packaged.S;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootTest
class BootFlowableApplicationTests {

    @Resource
    RepositoryService repositoryService;

    @Resource
    RuntimeService runtimeService;

    @Resource
    TaskService taskService;


    @Test
    void listTask(){
        //获取所有定义好的流程模型
        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().list();
        List<String> names = list.stream().map(ProcessDefinition::getName).collect(Collectors.toList());
        System.out.println(names);
    }

    @Test
    void studentLeave(){
        //让学生发起请假
        Map<String,Object> map = new HashMap<>();
        map.put("studentUser","小明");
        map.put("day",1);
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("StudentLeave", map);
        Task studentTask = taskService.createTaskQuery().planItemInstanceId(processInstance.getId()).singleResult();
        //让学生完成任务[发起请假]
        taskService.complete(studentTask.getId());
    }
    @Test
    void teacherTasks() {
        //获取teacher分组的任务
        List<Task> tasks=taskService.createTaskQuery().taskCandidateGroup("teacher").list();
        Map<String ,Object> map = new HashMap<>();
        map.put("outcome","通过");
        for (Task task:tasks){
            //获取任务相关的参数
            Map<String, Object> variables = taskService.getVariables(task.getId());
            String name = (String) variables.get("studentUser");
            String  day = (String) variables.get("day");
            System.out.printf("学生:%s,请假:%d天",name,day);
            //老师执行审批任务
            taskService.complete(task.getId(),map);
        }
    }

}
